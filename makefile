.PHONY: verify clean

MAIN_EXE=bin/sha512stdin

##########

${MAIN_EXE}: sha512stdin.c sha512.c sha512.h | bin
	gcc sha512stdin.c sha512.c -o $@ -std=c99 -Wall -Wextra -Wshadow

bin:
	mkdir bin

verify:
	frama-c -wp -wp-rte -wp-cache none sha512.c sha512.h

clean:
	rm -f ${MAIN_EXE}
