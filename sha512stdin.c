#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

#include "sha512.h"

/*@
	requires 0 <= number < 16;
	assigns \nothing;
	ensures
		('0' <= \result <= '9') ||
		('a' <= \result <= 'f');
*/
char HexDigitFromNumber(const int number) {
	if (number <= 9) {
		return '0' + number;
	}

	return 'a' - 10 + number;
}

void FprintHash(FILE *f, const uint64_t hash[const static SHA512_HASH_LENGTH_WORDS]) {
	char hexStr[SHA512_HASH_LENGTH_WORDS * 8 * 2 + 1];

	for (int i = 0; i < SHA512_HASH_LENGTH_WORDS; i += 1) {
		const int firstCharIdx = i * 8 * 2;
		const uint64_t word = hash[i];

		for (int o = 0; o < 8; o += 1) {
			const uint8_t octet = (word >> (8 * (7 - o))) & 0xff;

			hexStr[firstCharIdx + o * 2 + 0] = HexDigitFromNumber(octet >> 4);
			hexStr[firstCharIdx + o * 2 + 1] = HexDigitFromNumber(octet & 0xf);
		}
	}

	hexStr[SHA512_HASH_LENGTH_WORDS * 8 * 2 + 1 - 1] = '\0';

	fprintf(f, "%s", hexStr);
}

/*@
	assigns \nothing;
*/
int main(void) {
	uint64_t hash[SHA512_HASH_LENGTH_WORDS];
	sha512_Init(hash);

	uint8_t block[SHA512_BLOCK_FULL_LENGTH_OCTETS];
	uint64_t totalMessageLengthOctets = 0;

	while (true) {
		const size_t numRead = fread(block, sizeof(block[0]),
			SHA512_BLOCK_FULL_LENGTH_OCTETS, stdin);

		if (ferror(stdin) != 0) {
			return 1;
		}

		if (totalMessageLengthOctets > (SHA512_MAX_MESSAGE_LENGTH_OCTETS - numRead)) {
			fprintf(stderr, "The input to stdin is too long. "
				"It must not exceed %" PRIu64 " octets.\n",
				SHA512_MAX_MESSAGE_LENGTH_OCTETS);
			return 1;
		}

		totalMessageLengthOctets += numRead;

		if (numRead == SHA512_BLOCK_FULL_LENGTH_OCTETS) {
			sha512_HashFullBlock(hash, block);
		}
		else {
			sha512_HashLastBlock(hash, block, numRead, totalMessageLengthOctets);
			break;
		}
	}

	FprintHash(stdout, hash);
	fprintf(stdout, "\n");

	return 0;
}
