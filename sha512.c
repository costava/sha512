#include "sha512.h"

// Unit is 64-bit words.
#define SHA512_SCHEDULE_LENGTH_WORDS (80)

/*@ logic integer L_SHA512_ROTR(integer x, integer n) = ((x >> n) | (x << (64 - n))); */

// The ROTR (rotate right) operation from FIPS PUB 180-4 section 2.2.2 with w=64
/*
	require 0 <= n < 64;
	assigns \nothing;
	ensures \result == L_SHA512_ROTR(x, n);
*/
#define SHA512_ROTR(x, n) ((x >> n) | (x << (64 - n)))

/*@ logic integer UpperSigma1(integer e) =
	L_SHA512_ROTR(e, 14) ^
	L_SHA512_ROTR(e, 18) ^
	L_SHA512_ROTR(e, 41);
*/

/*@ logic integer Ch(integer e, integer f, integer g) =
	(e & f) ^ ((~e) & g);
*/

/*@
	requires \valid(&block[
		SHA512_BLOCK_FULL_LENGTH_OCTETS - 8
		..
		SHA512_BLOCK_FULL_LENGTH_OCTETS - 1
	]);

	assigns block[
		SHA512_BLOCK_FULL_LENGTH_OCTETS - 8
		..
		SHA512_BLOCK_FULL_LENGTH_OCTETS - 1
	];

	ensures block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 8] == ((numBits >> 56) & 0xff);
	ensures block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 7] == ((numBits >> 48) & 0xff);
	ensures block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 6] == ((numBits >> 40) & 0xff);
	ensures block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 5] == ((numBits >> 32) & 0xff);
	ensures block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 4] == ((numBits >> 24) & 0xff);
	ensures block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 3] == ((numBits >> 16) & 0xff);
	ensures block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 2] == ((numBits >>  8) & 0xff);
	ensures block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 1] == ((numBits >>  0) & 0xff);
*/
static inline void sha512_WriteNumBitsAtBlockEnd(
	uint8_t block[const static SHA512_BLOCK_FULL_LENGTH_OCTETS],
	const uint64_t numBits)
{
	block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 8] = (numBits >> 56) & 0xff;
	block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 7] = (numBits >> 48) & 0xff;
	block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 6] = (numBits >> 40) & 0xff;
	block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 5] = (numBits >> 32) & 0xff;
	block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 4] = (numBits >> 24) & 0xff;
	block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 3] = (numBits >> 16) & 0xff;
	block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 2] = (numBits >>  8) & 0xff;
	block[SHA512_BLOCK_FULL_LENGTH_OCTETS - 1] = (numBits >>  0) & 0xff;
}

void sha512_Init(uint64_t hash[const static SHA512_HASH_LENGTH_WORDS])
{
	hash[0] = 0x6a09e667f3bcc908;
	hash[1] = 0xbb67ae8584caa73b;
	hash[2] = 0x3c6ef372fe94f82b;
	hash[3] = 0xa54ff53a5f1d36f1;
	hash[4] = 0x510e527fade682d1;
	hash[5] = 0x9b05688c2b3e6c1f;
	hash[6] = 0x1f83d9abfb41bd6b;
	hash[7] = 0x5be0cd19137e2179;
}

void sha512_HashFullBlock(
	uint64_t hash[const restrict static SHA512_HASH_LENGTH_WORDS],
	const uint8_t block[const restrict static SHA512_BLOCK_FULL_LENGTH_OCTETS])
{
	uint64_t schedule[SHA512_SCHEDULE_LENGTH_WORDS];

	/*@
		loop invariant 0 <= i <= 16;
		loop assigns i, schedule[0 .. 15];
		loop variant 16 - i;
	*/
	for (uint_fast8_t i = 0; i <= 15 ; i += 1) {
		const uint_fast8_t firstOctetIdx = i * 8;

		// Looking at FIPS PUB 180-4:
		// Section 3.1 numbered list entry 4 tells us that SHA-512 operates on 64-bit _words_
		// Section 5.1.2 shows us that the first octet of an octet stream is the most significant octet of the first word.

		schedule[i] =
			(((uint64_t)block[firstOctetIdx + 0]) << UINT64_C(56)) |
			(((uint64_t)block[firstOctetIdx + 1]) << UINT64_C(48)) |
			(((uint64_t)block[firstOctetIdx + 2]) << UINT64_C(40)) |
			(((uint64_t)block[firstOctetIdx + 3]) << UINT64_C(32)) |
			(((uint64_t)block[firstOctetIdx + 4]) << UINT64_C(24)) |
			(((uint64_t)block[firstOctetIdx + 5]) << UINT64_C(16)) |
			(((uint64_t)block[firstOctetIdx + 6]) << UINT64_C( 8)) |
			(((uint64_t)block[firstOctetIdx + 7]) << UINT64_C( 0));
	}

	/*@
		loop invariant 16 <= i <= SHA512_SCHEDULE_LENGTH_WORDS;
		loop assigns i, schedule[16 .. SHA512_SCHEDULE_LENGTH_WORDS-1];
		loop variant SHA512_SCHEDULE_LENGTH_WORDS - i;
	*/
	for (uint_fast8_t i = 16; i < SHA512_SCHEDULE_LENGTH_WORDS; i += 1) {
		// Defined in section 4.1.3 of FIPS PUB 180-4:
		// LowerSigma0
		// LowerSigma1

		schedule[i] =
			(// LowerSigma1(schedule[i - 2])
				SHA512_ROTR(schedule[i - 2], 19) ^
				SHA512_ROTR(schedule[i - 2], 61) ^
				(schedule[i - 2] >> 6)
			) +
			schedule[i - 7] +
			(// LowerSigma0(schedule[i - 15])
				SHA512_ROTR(schedule[i - 15], 1) ^
				SHA512_ROTR(schedule[i - 15], 8) ^
				(schedule[i - 15] >> 7)
			) +
			schedule[i - 16];
	}

	uint64_t a = hash[0];
	uint64_t b = hash[1];
	uint64_t c = hash[2];
	uint64_t d = hash[3];
	uint64_t e = hash[4];
	uint64_t f = hash[5];
	uint64_t g = hash[6];
	uint64_t h = hash[7];

	{
		// Constants from FIPS PUB 180-4 section 4.2.3
		static const uint64_t sha512_K[80] = {
			0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,
			0x3956c25bf348b538, 0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118,
			0xd807aa98a3030242, 0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
			0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235, 0xc19bf174cf692694,
			0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
			0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
			0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4,
			0xc6e00bf33da88fc2, 0xd5a79147930aa725, 0x06ca6351e003826f, 0x142929670a0e6e70,
			0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
			0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b,
			0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30,
			0xd192e819d6ef5218, 0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8,
			0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8,
			0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,
			0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
			0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b,
			0xca273eceea26619c, 0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,
			0x06f067aa72176fba, 0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b,
			0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c,
			0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817
		};

		/*@
			loop invariant 0 <= i <= SHA512_SCHEDULE_LENGTH_WORDS;
			loop assigns i, a, b, c, d, e, f, g, h;
			loop variant SHA512_SCHEDULE_LENGTH_WORDS - i;
		*/
		for (uint_fast8_t i = 0; i < SHA512_SCHEDULE_LENGTH_WORDS; i += 1) {
			// Defined in section 4.1.3 of FIPS PUB 180-4:
			// Ch
			// Maj
			// UpperSigma0
			// UpperSigma1

			const uint64_t t1 =
				h +
				(// UpperSigma1(e)
					SHA512_ROTR(e, 14) ^
					SHA512_ROTR(e, 18) ^
					SHA512_ROTR(e, 41)
				) +
				(// Ch(e, f, g)
					(e & f) ^
					((~e) & g)
				) +
				sha512_K[i] +
				schedule[i];

			const uint64_t t2 =
				(// UpperSigma0(a)
					SHA512_ROTR(a, 28) ^
					SHA512_ROTR(a, 34) ^
					SHA512_ROTR(a, 39)
				) +
				(// Maj(a, b, c)
					(a & b) ^
					(a & c) ^
					(b & c)
				);

			h = g;
			g = f;
			f = e;
			e = d + t1;
			d = c;
			c = b;
			b = a;
			a = t1 + t2;
		}
	}

	hash[0] += a;
	hash[1] += b;
	hash[2] += c;
	hash[3] += d;
	hash[4] += e;
	hash[5] += f;
	hash[6] += g;
	hash[7] += h;
}

void sha512_HashLastBlock(
	uint64_t hash[const restrict static SHA512_HASH_LENGTH_WORDS],
	const uint8_t *const restrict block,
	const uint8_t blockLengthOctets,
	const uint64_t totalMessageLengthOctets)
{
	if (blockLengthOctets == 128) {
		sha512_HashFullBlock(hash, block);

		uint8_t builtBlock[SHA512_BLOCK_FULL_LENGTH_OCTETS];
		builtBlock[0] = 0x80;

		/*@
			loop invariant 1 <= i <= SHA512_BLOCK_FULL_LENGTH_OCTETS;
			loop invariant \forall uint_fast8_t j; (1 <= j < i) ==> (builtBlock[j] == 0);
			loop assigns i;
			loop assigns builtBlock[1 .. SHA512_BLOCK_FULL_LENGTH_OCTETS - 8 - 1];
			loop variant SHA512_BLOCK_FULL_LENGTH_OCTETS - 8 - i;
		*/
		for (uint_fast8_t i = 1; i < SHA512_BLOCK_FULL_LENGTH_OCTETS - 8; i += 1) {
			builtBlock[i] = 0;
		}

		sha512_WriteNumBitsAtBlockEnd(builtBlock, totalMessageLengthOctets * 8);
		sha512_HashFullBlock(hash, builtBlock);
	}
	else if (blockLengthOctets <= (SHA512_BLOCK_FULL_LENGTH_OCTETS - 17)) {
		uint8_t builtBlock[SHA512_BLOCK_FULL_LENGTH_OCTETS];

		/*@
			loop invariant 0 <= i <= blockLengthOctets;
			loop invariant \forall uint8_t j; (0 <= j < i) ==> (builtBlock[j] == block[j]);
			loop assigns i;
			loop assigns builtBlock[0 .. blockLengthOctets - 1];
			loop variant blockLengthOctets - i;
		*/
		for (uint8_t i = 0; i < blockLengthOctets; i += 1) {
			builtBlock[i] = block[i];
		}

		builtBlock[blockLengthOctets] = 0x80;

		/*@
			loop invariant blockLengthOctets + 1 <= i <= SHA512_BLOCK_FULL_LENGTH_OCTETS - 8;
			loop invariant \forall uint_fast8_t j; (blockLengthOctets + 1 <= j < i) ==> (builtBlock[j] == 0);
			loop assigns i;
			loop assigns builtBlock[blockLengthOctets + 1 .. SHA512_BLOCK_FULL_LENGTH_OCTETS - 8 - 1];
			loop variant SHA512_BLOCK_FULL_LENGTH_OCTETS - 8 - i;
		*/
		for (uint8_t i = blockLengthOctets + 1; i < SHA512_BLOCK_FULL_LENGTH_OCTETS - 8; i += 1) {
			builtBlock[i] = 0;
		}

		sha512_WriteNumBitsAtBlockEnd(builtBlock, totalMessageLengthOctets * 8);
		sha512_HashFullBlock(hash, builtBlock);
	}
	else {
		uint8_t builtBlock[SHA512_BLOCK_FULL_LENGTH_OCTETS];

		/*@
			loop invariant 0 <= i <= blockLengthOctets;
			loop invariant \forall uint8_t j; (0 <= j < i) ==> (builtBlock[j] == block[j]);
			loop assigns i;
			loop assigns builtBlock[0 .. blockLengthOctets - 1];
			loop variant blockLengthOctets - i;
		*/
		for (uint8_t i = 0; i < blockLengthOctets; i += 1) {
			builtBlock[i] = block[i];
		}

		builtBlock[blockLengthOctets] = 0x80;

		/*@
			loop invariant blockLengthOctets + 1 <= i <= SHA512_BLOCK_FULL_LENGTH_OCTETS;
			loop invariant \forall uint8_t j; (blockLengthOctets + 1 <= j < i) ==> (builtBlock[j] == 0);
			loop assigns i;
			loop assigns builtBlock[blockLengthOctets + 1 .. SHA512_BLOCK_FULL_LENGTH_OCTETS - 1];
			loop variant SHA512_BLOCK_FULL_LENGTH_OCTETS - i;
		*/
		for (uint8_t i = blockLengthOctets + 1; i < SHA512_BLOCK_FULL_LENGTH_OCTETS; i += 1) {
			builtBlock[i] = 0;
		}

		sha512_HashFullBlock(hash, builtBlock);

		/*@
			loop invariant 0 <= i <= SHA512_BLOCK_FULL_LENGTH_OCTETS - 8;
			loop invariant \forall uint_fast8_t j; (0 <= j < i) ==> (builtBlock[j] == 0);
			loop assigns i;
			loop assigns builtBlock[0 .. SHA512_BLOCK_FULL_LENGTH_OCTETS - 8 - 1];
			loop variant SHA512_BLOCK_FULL_LENGTH_OCTETS - 8 - i;
		*/
		for (uint_fast8_t i = 0; i < SHA512_BLOCK_FULL_LENGTH_OCTETS - 8; i += 1) {
			builtBlock[i] = 0;
		}

		sha512_WriteNumBitsAtBlockEnd(builtBlock, totalMessageLengthOctets * 8);
		sha512_HashFullBlock(hash, builtBlock);
	}
}
