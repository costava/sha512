#include "sha512.h"

// A main function for analysis by the Evolved Value Analysis (Eva) plugin of frama-c.

/*@
	assigns \nothing;
*/
int main(void) {
	uint64_t hash[SHA512_HASH_LENGTH_WORDS];
	sha512_Init(hash);

	// All values in array are 0.
	const uint8_t fullBlock[SHA512_BLOCK_FULL_LENGTH_OCTETS] = {0};

	const uint8_t block[] = {'a', 'b', 'c', '\n'};
	const uint64_t blockLenOctets = sizeof(block) / sizeof(uint8_t);

	const uint64_t totalMessageLengthOctets =
		SHA512_BLOCK_FULL_LENGTH_OCTETS + blockLenOctets;

	sha512_HashFullBlock(hash, fullBlock);
	sha512_HashLastBlock(hash, block, blockLenOctets, totalMessageLengthOctets);

	return 0;
}
