#ifndef SHA512_H
#define SHA512_H

#include <stdint.h>

// An implementation of SHA-512 as defined in:
// FIPS PUB 180-4
// https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.180-4.pdf

// Length of the hash digest (result) in 64-bit words.
#define SHA512_HASH_LENGTH_WORDS (8)
#define SHA512_BLOCK_FULL_LENGTH_OCTETS (128)
#define SHA512_MAX_MESSAGE_LENGTH_OCTETS (UINT64_MAX / 8)

/*@
	requires \valid(&hash[0 .. (SHA512_HASH_LENGTH_WORDS - 1)]);
	assigns hash[0 .. (SHA512_HASH_LENGTH_WORDS - 1)];

	ensures hash[0] == 0x6a09e667f3bcc908;
	ensures hash[1] == 0xbb67ae8584caa73b;
	ensures hash[2] == 0x3c6ef372fe94f82b;
	ensures hash[3] == 0xa54ff53a5f1d36f1;
	ensures hash[4] == 0x510e527fade682d1;
	ensures hash[5] == 0x9b05688c2b3e6c1f;
	ensures hash[6] == 0x1f83d9abfb41bd6b;
	ensures hash[7] == 0x5be0cd19137e2179;
*/
void sha512_Init(uint64_t hash[const static SHA512_HASH_LENGTH_WORDS]);

/*@
	requires \valid(&hash[0 .. (SHA512_HASH_LENGTH_WORDS - 1)]);
	requires \valid_read(&block[0 .. (SHA512_BLOCK_FULL_LENGTH_OCTETS - 1)]);
	requires \separated(
		&hash[0 .. (SHA512_HASH_LENGTH_WORDS - 1)],
		&block[0 .. (SHA512_BLOCK_FULL_LENGTH_OCTETS - 1)]
	);
	assigns hash[0 .. (SHA512_HASH_LENGTH_WORDS - 1)];
*/
void sha512_HashFullBlock(
	uint64_t hash[const restrict static SHA512_HASH_LENGTH_WORDS],
	const uint8_t block[const restrict static SHA512_BLOCK_FULL_LENGTH_OCTETS]);

/*@
	requires \valid(&hash[0 .. (SHA512_HASH_LENGTH_WORDS - 1)]);
	requires \valid_read(&block[0 .. (blockLengthOctets - 1)]);
	requires \separated(
		&hash[0 .. (SHA512_HASH_LENGTH_WORDS - 1)],
		&block[0 .. (blockLengthOctets - 1)]
	);
	requires 0 <= blockLengthOctets <= SHA512_BLOCK_FULL_LENGTH_OCTETS;
	requires 0 <= totalMessageLengthOctets <= SHA512_MAX_MESSAGE_LENGTH_OCTETS;
	assigns hash[0 .. (SHA512_HASH_LENGTH_WORDS - 1)];
*/
void sha512_HashLastBlock(
	uint64_t hash[const restrict static SHA512_HASH_LENGTH_WORDS],
	const uint8_t *const restrict block,
	const uint8_t blockLengthOctets,
	const uint64_t totalMessageLengthOctets);

#endif // SHA512_H
