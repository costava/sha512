# sha512

Annotated SHA-512 implementation in C99.  
Example usage can be seen in `sha512stdin.c` which hashes the contents from stdin.  
e.g.
```
$ echo "abc" | bin/sha512stdin 
4f285d0c0cc77286d8731798b7aae2639e28270d4166f40d769cbbdca5230714d848483d364e2f39fe6cb9083c15229b39a33615ebc6d57605f7c43f6906739d
$ echo "abc" | sha512sum
4f285d0c0cc77286d8731798b7aae2639e28270d4166f40d769cbbdca5230714d848483d364e2f39fe6cb9083c15229b39a33615ebc6d57605f7c43f6906739d  -
$
```

## Annotations

The functions are annotated with the ANSI/ISO C Specification Language (ACSL).  
The annotations can be verified with `make verify`

All 280 goals are verified successfully.

Tested with:
- frama-c 25.0 (Manganese)
- alt-ergo 2.4.0
